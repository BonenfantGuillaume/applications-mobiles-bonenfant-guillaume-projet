package fr.uavignon.ceri.project.data;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

@Entity(tableName = "museum_table", indices = {@Index(value = {"name", "description"},
        unique = true)})
public class Artefact {

    public static final String TAG = Artefact.class.getSimpleName();

    public static final String ADD_ID = "";

    @PrimaryKey
    @NonNull
    @ColumnInfo(name="_id")
    private String id;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @NonNull
    @ColumnInfo(name="description")
    private String description;

    @ColumnInfo(name="brand")
    private String brand;

    @ColumnInfo(name="year")
    private Integer year;

    @ColumnInfo(name="working")
    private Boolean working;

    @ColumnInfo(name="lastUpdate")
    private long lastUpdate; // Last time when data was updated (Unix time)

    @Ignore
    public Artefact(@NonNull String name, @NonNull String country) {
        this.name = name;
    }

    @Ignore
    public Artefact(@NonNull String name, String description, long lastUpdate) {
        this.name = name;
        this.description = description;
        this.lastUpdate = lastUpdate;
    }

    public Artefact(@NonNull String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getBrand() {
        return brand;
    }

    public Integer getYear() {
        return year;
    }

    public Boolean getWorking() { return working; }

    public long getLastUpdate() {
        return lastUpdate;
    }
    public String getStrLastUpdate() {
        Date date = new Date(lastUpdate*1000L);
        DateFormat shortDateFormat = DateFormat.getDateTimeInstance(
                DateFormat.SHORT,
                DateFormat.SHORT, new Locale("FR","fr"));

        return shortDateFormat.format(date);
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) { this.description = description; }

    public void setBrand(String brand) { this.brand = brand; }

    public void setYear(Integer year) { this.year = year; }

    public void setWorking(Boolean working) { this.working = working; }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

}
