package fr.uavignon.ceri.project;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.project.data.Artefact;
import fr.uavignon.ceri.project.data.webservice.MuseumRepository;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private MuseumRepository repository;
    private MutableLiveData<Artefact> artefact;

    public DetailViewModel (Application application) {
        super(application);
        repository = MuseumRepository.get(application);
        artefact = new MutableLiveData<>();
    }

    public void setArtefact(String id) {
        repository.getArtefact(id);
        artefact = repository.getSelectedArtefact();
    }

    LiveData<Artefact> getArtefact() {
        return artefact;
    }
}

