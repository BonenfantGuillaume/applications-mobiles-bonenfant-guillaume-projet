package fr.uavignon.ceri.project.data.webservice;

import fr.uavignon.ceri.project.data.Artefact;

public class MuseumResult {
    public final boolean isLoading;
    public final Throwable error;

    MuseumResult(boolean isLoading, Throwable error) {
        this.isLoading = isLoading;
        this.error = error;
    }

    static void transferInfo(MuseumResponse artefactInfo, Artefact artefact)
    {
        artefact.setName(artefactInfo.name);
        artefact.setDescription(artefactInfo.description);
        artefact.setBrand(artefactInfo.brand);
        artefact.setYear(artefactInfo.year);
        artefact.setWorking(artefactInfo.working);
    }

}
