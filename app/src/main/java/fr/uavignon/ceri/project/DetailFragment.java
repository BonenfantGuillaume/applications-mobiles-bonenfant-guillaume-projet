package fr.uavignon.ceri.project;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.snackbar.Snackbar;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView textArtefactName, textDescription, textYear, textWorking, textBrand, textLastUpdate;
    private ImageView imgWeather;
    private ProgressBar progress;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // Get selected artefact
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        String artefactID = args.getArtefactId();
        Log.d(TAG,"selected id="+artefactID);
        viewModel.setArtefact(artefactID);

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {
        textArtefactName = getView().findViewById(R.id.nameArtefact);
        textDescription = getView().findViewById(R.id.editDescription);
        textYear = getView().findViewById(R.id.editYear);
        textWorking = getView().findViewById(R.id.editEtat);
        textBrand = getView().findViewById(R.id.editBrand);

        imgWeather = getView().findViewById(R.id.iconeWeather);

        progress = getView().findViewById(R.id.progress);

        getView().findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Interrogation à faire du service web",
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
            }
        });

        getView().findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.project.DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }

    private void observerSetup() {
        viewModel.getArtefact().observe(getViewLifecycleOwner(),
                artefact -> {
                    if (artefact != null) {
                        Log.d(TAG, "observing city view");

                        if(artefact.getName() != null)
                        {
                            textArtefactName.setText(artefact.getName());
                        }
                        Glide.with(this.getActivity())
                                .load("https://demo-lia.univ-avignon.fr/cerimuseum//items/"+artefact.getId()+"/thumbnail")
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .into(imgWeather);

                        if(artefact.getDescription() != null)
                        {
                            textDescription.setText(artefact.getDescription());
                        }

                        if(artefact.getYear() != null)
                        {
                            textYear.setText(artefact.getYear() + "");
                        }
                        else
                        {
                            textYear.setText("Inconnue");
                        }

                        if(artefact.getBrand() != null)
                        {
                            textBrand.setText(artefact.getBrand());
                        }
                        else
                        {
                            textBrand.setText("Inconnue");
                        }

                        if(artefact.getWorking() == null)
                        {
                            textWorking.setText("Inconnu");
                        }
                        else if (artefact.getWorking() == false)
                        {
                            textWorking.setText("Non fonctionnel");
                        }
                        else
                        {
                             textWorking.setText("Fonctionnel");
                        }
                    }
                });
    }
}