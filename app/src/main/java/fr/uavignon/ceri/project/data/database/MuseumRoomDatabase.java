package fr.uavignon.ceri.project.data.database;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.uavignon.ceri.project.data.Artefact;

@Database(entities = {Artefact.class}, version = 6, exportSchema = false)
public abstract class MuseumRoomDatabase extends RoomDatabase {

    private static final String TAG = MuseumRoomDatabase.class.getSimpleName();

    public abstract ArtefactDao artefactDao();

    private static MuseumRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public static MuseumRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MuseumRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                            // without populate

                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    MuseumRoomDatabase.class,"museum_database")
                                    .fallbackToDestructiveMigration()
                                    .build();



                    // with populate
                    /*INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(),
                        WeatherRoomDatabase.class,"book_database")
                        .addCallback(sRoomDatabaseCallback)
                        .build();*/

                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);

                    databaseWriteExecutor.execute(() -> {
                        // Populate the database in the background.
                        ArtefactDao dao = INSTANCE.artefactDao();
                        dao.deleteAll();

                        Artefact[] artefacts = {new Artefact("Avignon", "France"),
                        new Artefact("Paris", "France"),
                        new Artefact("Rennes", "France"),
                        new Artefact("Montreal", "Canada"),
                        new Artefact("Rio de Janeiro", "Brazil"),
                        new Artefact("Papeete", "French Polynesia"),
                        new Artefact("Sydney", "Australia"),
                        new Artefact("Seoul", "South Korea"),
                        new Artefact("Bamako", "Mali"),
                        new Artefact("Istanbul", "Turkey")};

                        for(Artefact newArtefact : artefacts)
                            dao.insert(newArtefact);
                        Log.d(TAG,"database populated");
                    });

                }
            };



}
