package fr.uavignon.ceri.project.data.webservice;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface OWMInterface {

    @Headers("Accept: application/json; charset=utf-8")
    @GET("museum")
    Call<MuseumResponse> getForecast(@Query("type") String query);

    @Headers({
            "Accept: json",
    })
    @GET("collection")
    public Call<Map<String, MuseumResponse>> getCollection();
}
