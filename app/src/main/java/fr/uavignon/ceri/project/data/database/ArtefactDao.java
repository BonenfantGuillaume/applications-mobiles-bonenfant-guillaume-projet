package fr.uavignon.ceri.project.data.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.project.data.Artefact;

@Dao
public interface ArtefactDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Artefact artefact);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertOrUpdate(Artefact artefact);

    @Query("DELETE FROM museum_table")
    void deleteAll();

    @Query("SELECT * from museum_table ORDER BY name ASC")
    LiveData<List<Artefact>> getAllArtefacts();

    @Query("SELECT * from museum_table ORDER BY name ASC")
    List<Artefact> getSynchrAllArtefacts();

    @Query("DELETE FROM museum_table WHERE _id = :id")
    void deleteArtefact(String id);

    @Query("SELECT * FROM museum_table WHERE _id = :id")
    Artefact getArtefactById(String id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Artefact artefact);
}
