package fr.uavignon.ceri.project;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import fr.uavignon.ceri.project.data.Artefact;
import fr.uavignon.ceri.project.data.webservice.MuseumRepository;

public class ListViewModel extends AndroidViewModel {
    private MuseumRepository repository;
    private LiveData<List<Artefact>> allArtefacts;

    public ListViewModel (Application application) {
        super(application);
        repository = MuseumRepository.get(application);
        allArtefacts = repository.getAllArtefacts();
    }

    LiveData<List<Artefact>> getAllArtefacts() {
        return allArtefacts;
    }

    public void deleteArtefact(String id) {
        repository.deleteArtefact(id);
    }

    public void loadData() { repository.loadData(); }
}
