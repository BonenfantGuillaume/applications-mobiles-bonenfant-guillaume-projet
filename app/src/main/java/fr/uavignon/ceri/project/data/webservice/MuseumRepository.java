package fr.uavignon.ceri.project.data.webservice;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.project.data.Artefact;
import fr.uavignon.ceri.project.data.database.ArtefactDao;
import fr.uavignon.ceri.project.data.database.MuseumRoomDatabase;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.project.data.database.MuseumRoomDatabase.databaseWriteExecutor;

public class MuseumRepository {

    private static final String TAG = MuseumRepository.class.getSimpleName();

    private LiveData<List<Artefact>> allArtefacts;
    private MutableLiveData<Artefact> selectedArtefact;

    private ArtefactDao artefactDao;
    private final OWMInterface api;


    private static volatile MuseumRepository INSTANCE;

    public synchronized static MuseumRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new MuseumRepository(application);
        }

        return INSTANCE;
    }

    public MuseumRepository(Application application) {
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(OWMInterface.class);

        MuseumRoomDatabase db = MuseumRoomDatabase.getDatabase(application);
        artefactDao = db.artefactDao();
        allArtefacts = artefactDao.getAllArtefacts();
        selectedArtefact = new MutableLiveData<>();
    }

    public LiveData<List<Artefact>> getAllArtefacts() {
        return allArtefacts;
    }

    public MutableLiveData<Artefact> getSelectedArtefact() {
        return selectedArtefact;
    }

    public void loadData() {

        api.getCollection().enqueue(
                new Callback<Map<String, MuseumResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String, MuseumResponse>> call,
                                           Response<Map<String, MuseumResponse>> response) {

                        for (String artefactId: response.body().keySet()) {
                            Artefact artefact= new Artefact(artefactId);
                            MuseumResult.transferInfo(response.body().get(artefactId), artefact);
                            insertOrUpdate(artefact);
                        }
                        //getObject("ave");
                        //Log.d(TAG, "onResponse: " + selectedObject.getValue().getName());
                    }

                    public void onFailure(Call<Map<String, MuseumResponse>> call, Throwable t) {
                        Log.d("FAILURE API",t.getMessage());
                    }
                }
        );
    }



    public long insertArtefact(Artefact newArtefact) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return artefactDao.insert(newArtefact);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedArtefact.setValue(newArtefact);
        return res;
    }

    public long insertOrUpdate(Artefact newArtefact) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return artefactDao.insertOrUpdate(newArtefact);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;
    }

    public int updateArtefact(Artefact artefact) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return artefactDao.update(artefact);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedArtefact.setValue(artefact);
        return res;
    }

    public void deleteArtefact(String id) {
        databaseWriteExecutor.execute(() -> {
            artefactDao.deleteArtefact(id);
        });
    }

    public void getArtefact(String id)  {
        Future<Artefact> fartefact = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return artefactDao.getArtefactById(id);
        });
        try {
            selectedArtefact.setValue(fartefact.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }




}
